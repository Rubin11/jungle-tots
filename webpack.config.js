const path = require('path');
module.exports = {
    mode: "development",
    entry: "./config/index.js",
    output: {
        filename: "app.js",
        path: path.resolve(__dirname + "/assets", "js")
    },
    module: {
      rules: [
        {
          test: /\.s[ac]ss$/i,
          use: ['style-loader', 'css-loader?url=false', 'postcss-loader', 'sass-loader',
          ],
        },
      ],
    },
  };