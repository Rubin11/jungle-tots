header {
    position: relative;
    height: 557px;
    background-color: $green;

    .container {
        position: relative;
        height: 100%;
        z-index: 6;

        &.main {
        
            @media (min-width: 1550px) {
                max-width: 1411px;
            }
        }
    }

    .header--vectors {
        position: absolute;
        top: 0;
        height: 100%;
        left: 0;
        width: 100%;
        z-index: 5;

        .header--leaf {
            position: absolute;
            bottom: 22px;
            left: -135px;
            z-index: 0;
            transform: rotate(210deg) scaleX(-1);
            display: none;

            @media (min-width: 560px) {
                display: block;
            }
        }
        .header--bird {
            position: absolute;
            bottom: 35px;
            left: -71px;
            z-index: 4;
            display: none;

            @media (min-width: 560px) {
                display: block;
            }
        }
    }

    .header--bg {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0px;
        background-position: center top;
        background-image: url(assets/images/header-bg.png);
        background-repeat: no-repeat;
        z-index: 0;
    }

    .header--main {
        position: relative;
        top: -230px;
        z-index: 3;
        max-width: 100%;
        margin: 0;

        @media (min-width: 768px) {
            top: -300px;
        }
        @media (min-width: 1200px) {
            top: -360px;
        }
        @media (min-width: 1200px) {
            top: -400px;
        }


         .col {
            display: flex;
            justify-content: center;
            align-items: center;
         }
    }
    .header--top {
        padding-top: 23px;
        margin-bottom: 15px;
        text-align: center;
        
        @media (min-width: 992px) {
            text-align: left;
        }

        &:after {
            @media (min-width: 992px) {
                content: "";
                width: 212px;
                height: 116px;
                background-image: url(assets/images/header-leaf.svg);
                background-position: center top;
                position: absolute;
                background-size: cover;
                background-repeat: no-repeat;
                top: -23px;
                right: -150px;
                z-index: -1;
                animation: leaf_shake 20s ease-in-out infinite;
            }
            @media (min-width: 1550px) {
                position: absolute;
                right: -130px;
            }
        }
        .site-logo {
            transition: .4s all cubic-bezier(0.98, 0.2, 0.65, 1.03) 1s;
            position: relative;
            top: -300px;
            opacity: 0;
        }
        .header--top-welcome {
            order: 3;
            top: 90px;

            @media (min-width: 768px) {
                order: unset;
                top: 0px;
            }
        }

        .header--top-social {
            position: relative;
            margin-top: -80px;

            @media (min-width: 768px) {
                margin-top: -210px;
            }
            @media (min-width: 992px) {
                margin-top: 0px;
            }
        }


        img {
            max-width: 142px;
            
            @media (min-width: 768px) {
                max-width: 202px;
            }
            @media (min-width: 992px) {
                max-width: none;
            }
        }

        .header--welcome {
            padding-top: 35px;
            font-family: $museo;
            color: $lGreen;
            font-size: 14px;

            @media (min-width: 992px) {
                display: block;
            }
            @media (min-width: 1200px) {
                font-size: 17px;
            }

            strong {
                font-family: $arvo;
                font-size: 24px;
                color: white;

                @media (min-width: 1200px) {
                    font-size: 28px;
                }
                @media (min-width: 1550px) {
                    font-size: 28px;
                }
            }
        }
        .header--info {
            &-top {
                .header--info-social {
                    padding: 10px 15px;
                    display: flex;
                    justify-content: flex-end;

                    @media (min-width: 768px) {
                        position: relative;
                        right: -20px;
                    }
                    @media (min-width: 992px) {
                        padding-left: 123px;
                        right: 0px;
                    }
                    @media (min-width: 1200px) {
                        padding-left: 153px;
                    }
                    @media (min-width: 1550px) {
                        padding-left: 190px;
                    }
                    
                    a {
                        background-color: $dGreen;
                        color: white;
                        display: inline-flex;
                        justify-content: center;
                        align-items: center;
                        width: 34px;
                        height: 34px;
                        margin-right: 5px;
                        transition: $btnTrans;

                        @media (min-width: 992px) {
                            width: 27px;
                            height: 27px;
                            font-size: 13px;
                            margin-right: 5px;
                        }
                        @media (min-width: 1550px) {
                            width: 34px;
                            height: 34px;
                            font-size: 16px;
                            margin-right: 5px;
                        }

                        &:hover {
                            background-color: white;
                            color: $dGreen;
                        }
                    }
                }
            }
            .header--info-bottom {
                display: flex;
                justify-content: flex-end;
                padding: 0 20px;
                align-items: center;

                @media (min-width: 768px) {
                    padding: 0;
                    justify-content: flex-end;
                }
                @media (min-width: 992px) {
                    justify-content: space-around;
                }
                

                .header--info-number {
                    font-size: 21px;
                    font-family: $arvo;
                    font-weight: bold;
                    color: white;

                    @media (min-width: 992px) {
                        font-size: 15px;
                    }
                    @media (min-width: 1200px) {
                        font-size: 17px;
                    }
                    @media (min-width: 1550px) {
                        font-size: 21px;
                    }
                    
                    a {
                        color: white;
                        transition: $btnTrans;

                        &:hover {
                            color: $lGreen;
                        }
                    }
                }
                .header--info-cta {
                    position: absolute;
                    top: 110px;
                    left: 0;
                    width: 100%;
                    display: flex;
                    justify-content: center;

                    @media (min-width: 768px) {
                        position: relative;
                        top: 0px;
                        left: 0;
                        width: auto;
                        display: block;
                        margin-left: 15px;
                    }

                    @media (min-width: 992px) {
                        height: 45px;
                        padding: 0px 15px;
                        font-size: 11.5px;
                        margin-left: 0;
                    }
                    @media (min-width: 1200px) {
                        height: 50px;
                        font-size: 13.5px;
                        padding: 0px;
                    }
                    @media (min-width: 1550px) {
                        height: 63px;
                        font-size: 17.5px;
                    }

                    a {
                        height: 63px;
                        background-color: $lGreen;
                        font-family: $arvo;
                        padding: 0px 20px;
                        font-size: 17.5px;
                        color: $green;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        font-weight: bold;
                        transition: $btnTrans;

                        &:hover {
                            background-color: $dGreen;
                            color: $lGreen;
                        }

                        @media (min-width: 992px) and (max-width: 1549px) {
                            height: 50px;
                            background-color: #6CBE45;
                            font-family: "Arvo", serif;
                            padding: 0px 12px;
                            font-size: 11.5px;
                        }
                        
                    }
                }
            }
        }
    }
} 